package Lab01;


public class User {

    
    public static void main(String[] args) {
        
        WaterDispenser w1 = new WaterDispenser();
        
        w1.powerOnOff();
        w1.fillTank();
        w1.waterButton();
        w1.powerOnOff();
    }
    
}
