package Lab01;


public class WaterDispenser {
    private int waterLevel;
    private boolean powerOn;
    private boolean lidOn;
    private int cup;
    
    public WaterDispenser(){
        this.waterLevel = 0;
        this.powerOn = false;
        this.lidOn = false;
        this.cup = 100;
    }
    
    public void waterButton(){
        if(powerOn = true){
            if(waterLevel >= cup){
                this.waterLevel = this.waterLevel - cup;
                System.out.println("Enjoy your water!");
            }
            else{
                System.out.println("Not enough water in the tank.");
            }
        }
        else{
            System.out.println("Power is not on.");
        }
    }
    
    public void powerOnOff(){
        this.powerOn = !this.powerOn;
        System.out.println("Beep!");
    }
    public void fillTank(){
        this.waterLevel = 10000;
        System.out.println("The tank is now full.");
    }
}
