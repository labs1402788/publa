package Lab02;


public class CoffeeMaster {
    private int waterLevel;
    private int beans;
    private boolean isOn;
    private int rinse; //uses 25ml of water
    private int clean; // uses 300ml of water
    private int cc; //coffee count
    private int cw; //amount of water used in making coffee
    private int cb; //amount of beans used in making coffee
    private int ew; //amount of water used in making espresso 
    private int eb; //amount of beans used in making espresso
    
    public CoffeeMaster(){
        this.waterLevel = 0;
        this.beans = 0;
        this.isOn = false;
        this.rinse = 25;
        this.clean = 300;
        this.cc = 0;
        this.cw = 150;
        this.cb = 10;
        this.ew = 30;
        this.eb = 8;
        
    }
    public void powerOnOff(){
        this.isOn = !this.isOn;
        System.out.println("Beep!");
        System.out.println("Rinsing..");
        this.waterLevel = this.waterLevel - rinse;
    }
    public void fillWater(){
        this.waterLevel = 2000;
        System.out.println("The watertank is now filled.");
    }
    public void fillBeans(){
        this.beans = 1000;
        System.out.println("The beans are now filled.");
    }
    public void coffeeButton(){
        if(isOn = true){
            if(cc < 10){
                if((waterLevel >= cw) && (beans >= cb)){    // check the beans & water to see if you have enough
                    this.waterLevel = this.waterLevel - cw;
                    this.beans = this.beans - cb;   // decrease the amount of beans.
                    System.out.println("Enjoy your coffee!");
                    cc++; //increase the coffee count
                }
                else{
                    System.out.println("Not enough water or beans in the tank.");
                }
            }
                else{
                System.out.println("You have to clean the system first.");
                }
        }
        else{
            System.out.println("Power is not on.");
        }
    }
    public void espressoButton(){
        if(isOn = true){
            if (cc <10){
                if((waterLevel >= ew) && (beans >= eb)){ // check the beans & water to see if you have enough
                    this.waterLevel = this.waterLevel - ew; //decrease the amount of water.
                    this.beans = this.beans - eb; // decrease the amount of beans.
                    System.out.println("Enjoy your espresso!");
                    cc++; //increase the coffee count
                }
                else{
                    System.out.println("Not enough water or beans in the tank.");
                }
            }
            else{
                System.out.println("You have to clean the system first.");
            }
        }
        else{
            System.out.println("Power is not on.");
        }
    }
    
    public void cleanMaster(){
        this.waterLevel = this.waterLevel - clean;  //decrease the amount of water.
        this.cc = 0;    // reset the coffee counter
        System.out.println("The system is now clean and operable.");
    }
    
}
