package Lab02;


public class User {

    
    public static void main(String[] args) {
        CoffeeMaster cm1 = new CoffeeMaster();
        
        cm1.fillBeans();
        cm1.fillWater();
        cm1.powerOnOff();
        cm1.coffeeButton();
        cm1.espressoButton();
        cm1.cleanMaster();
        cm1.powerOnOff();
    }
    
}
